<?php

require 'bootstrap.php';

if ($_SERVER["REQUEST_METHOD"] == 'POST' && !empty($_POST["username"]) && !empty($_POST["password"])) {
  $username = $_POST["username"];
  $password = $_POST["password"];
  $user = User::createUser($username, $password);

  if ($user === null) {
    header("register.php?message=" . urlencode('User already exists!!!'));
  } elseif ($user === false || $user) {
    unset($_POST["username"], $_POST["password"]);
    header("Location: login.php");
    exit;
  }
}

require 'includes/header.php';
?>
<div class="container tm-mt-big tm-mb-big">
  <div class="row">
    <div class="col-12 mx-auto tm-login-col">
      <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
        <div class="row">
          <div class="col-12 text-center">
            <?php if (isset($_GET['message'])) : ?>
              <p class="text-danger"><?= $_GET['message']; ?></p>
            <?php endif ?>
            <h2 class="tm-block-title mb-4">Welcome to Dashboard, Register</h2>
          </div>
        </div>
        <div class="row mt-2">
          <div class="col-12">
            <form action="" method="POST" class="tm-login-form">
              <div class="form-group">
                <label for="username">Username</label>
                <input name="username" type="text" class="form-control validate" id="username" required />
              </div>
              <div class="form-group mt-3">
                <label for="password">Password</label>
                <input name="password" type="password" class="form-control validate" id="password" required />
              </div>
              <div class="form-group mt-4">
                <button type="submit" class="btn btn-primary btn-block text-uppercase">
                  Sign Up
                </button>
              </div>
              <div class="mb-3">
                <h5 class="mt-5 text-center text-warning">Do you have an account?</h5>
                <a href="login.php" class="mt-3 btn btn-primary btn-block text-uppercase">
                  Sign In
                </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require 'includes/footer.php';