<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login']) {
  header("Location: login.php");
  exit();
}

if ($_COOKIE["role"] !== "admin") {
  header("Location: index.php");
  exit();
}

$page = "Edit Stock";

if ($_SERVER["REQUEST_METHOD"] == 'POST' && isset($_POST["PUT"]) && isset($_POST["category_ids"]) && isset($_POST["stock_id"]) && isset($_POST["stock_name"])) {
  $stock_id = intval($_POST["stock_id"]);
  $new_category_ids = $_POST["category_ids"];
  $new_stock_name = $_POST["stock_name"];
  $old_stock_categories = Pivot::getAllCategoriesByStockId($stock_id);

  Stock::updateStockById($stock_id, $new_stock_name);

  function obj_to_arr()
  {
    global $old_stock_categories;
    $old_stock_category_ids = [];
    foreach ($old_stock_categories as $category) {
      $old_stock_category_ids[] = $category->id;
    }

    return $old_stock_category_ids;
  }

  $deleted_categories = array_filter($old_stock_categories, fn ($el) => in_array($el->id, $_POST["category_ids"]) ? false : true);
  foreach ($deleted_categories as $deleted_category) {
    Pivot::deleteStockByCategoryId($stock_id, $deleted_category->id);
  }

  $updated_stock_categories = array_filter($new_category_ids, fn ($el) => in_array($el, obj_to_arr()) ? false : true);
  foreach ($updated_stock_categories as $updated_category) {
    Pivot::addNewStockWithCategory($stock_id, $updated_category->id);
  }

  unset($_POST["stock_id"]);
  unset($_POST["category_ids"]);
  unset($_POST["stock_name"]);

  header("Location: stocks.php");
  exit;
}

if ($_SERVER["REQUEST_METHOD"] == 'POST' && isset($_POST["PUT"]) && isset($_POST["stock_id"])) {
  $categories = Category::getAllCategories();

  $stock_id = $_POST["stock_id"];
  $stock = Stock::getStockById($stock_id);
  $stock_categories = Pivot::getAllCategoriesByStockId($stock_id);

  function filtered_category($el)
  {
    global $stock_categories;
    return in_array($el, $stock_categories) ? false : true;
  }

  $filtered_categories = array_filter($categories, "filtered_category");

  unset($_POST["stock_id"]);
}

require 'includes/header.php';
?>
<div class="container tm-mt-big tm-mb-big">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mx-auto">
      <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
        <div class="row">
          <div class="col-12">
            <h2 class="tm-block-title d-inline-block">Edit Stock</h2>
          </div>
        </div>
        <div class="row tm-edit-product-row">
          <div class="col-xl-12 col-lg-12 col-md-12">
            <form action="" method="POST" class="tm-edit-product-form">
              <input type="hidden" name="PUT" />
              <input type="hidden" name="stock_id" value="<?= $stock->id; ?>" />
              <div class="form-group mb-3">
                <label for="name">Stock Name</label>
                <input id="name" name="stock_name" value="<?= $stock->stock_name; ?>" type="text" />
              </div>
              <div class="form-group mb-3">
                <label for="category">Stock Categories</label>
                <select class="custom-select tm-select-accounts" name="category_ids[]" id="category" multiple>
                  <?php foreach ($stock_categories as $stock_category) : ?>
                    <option selected value="<?= $stock_category->id; ?>"><?= $stock_category->category_name; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="form-group mb-3">
                <label for="category">Add Category</label>
                <select class="custom-select tm-select-accounts" name="category_ids[]" id="category" multiple>
                  <?php foreach ($filtered_categories as $category) : ?>
                    <option value="<?= $category->id; ?>"><?= $category->category_name; ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div>
                <button type="submit" class="btn btn-primary btn-block text-uppercase">Update Stock</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php require 'includes/footer.php';