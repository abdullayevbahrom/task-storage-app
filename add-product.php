<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login']) {
  header("Location: login.php");
  exit();
}
if ($_COOKIE["role"] == "user") {
  header("Location: index.php");
  exit();
}

$page = "Add Product";

if ($_SERVER["REQUEST_METHOD"] == 'POST' && isset($_POST["name"]) && isset($_POST["price"])) {
  $name = $_POST["name"];
  $price = $_POST["price"];

  Product::addProduct($name, $price);

  unset($_POST["name"], $_POST["price"]);

  header("Location: products.php");
  exit;
}

require 'includes/header.php';
?>
<div class="container tm-mt-big tm-mb-big">
  <div class="row">
    <div class="col-xl-9 col-lg-10 col-md-12 col-sm-12 mx-auto">
      <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
        <div class="row">
          <div class="col-12">
            <h2 class="tm-block-title d-inline-block">Add Product</h2>
          </div>
        </div>
        <div class="row tm-edit-product-row">
          <div class="col-xl-12 col-lg-12 col-md-12">
            <form action="" method="POST" class="tm-edit-product-form">
              <div class="form-group mb-3">
                <label for="name">Name</label>
                <input id="name" name="name" type="text" class="form-control validate" required />
              </div>
              <div class="form-group mb-3">
                <label for="price">Price</label>
                <input id="price" name="price" type="number" class="form-control validate" required />
              </div>
              <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block text-uppercase">Add Product Now</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require 'includes/footer.php';