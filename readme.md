# task storage-app


App for Storage with
[PHP](https://www.php.net), [MySQL](https://www.mysql.com)

App has 3 docker containers: **php**, **mysql** and **adminer** 

<p align="center"><a href="https://php.net" target="_blank"><img src="https://www.freepnglogos.com/uploads/logo-php-png/php-jon-dennis-web-design-graphic-design-support-14.png" alt="PHP logo"></a></p>

![poster](task.jpg)<br/>
![poster](dashboard.png)<br/>
![poster](login.png)<br/>
![poster](input.png)<br/>
![poster](db_design.png)<br/>

## Installation

Clone the project<br>
```https://gitlab.com/abdullayevbahrom/task-storage-app.git```

Go to the project directory<br>
```cd task-storage-app```

Run docker containers <br>
```docker-compose up -d```

**Done! You can open <a href="http://localhost:9000" target="_blank">http://localhost:9000</a> via browser.

There are 3 roles in this app. These are ```admin```, ```manager```, ```user```

**Admin has all rights and can grant rights to other roles.

**Manager can add products, view reports.

**User is an ordinary user who only watches the site.

DEFAULT INITIALIZED USERS:

**ADMIN
Username: ```admin``` Password: ```adminpassword```

**MANAGER
Username: ```manager``` Password: ```managerpassword```

**ADMIN
Username: ```user``` Password: ```userpassword```


## Docker
For enter to php container run 
```docker-compose exec php bash```

For enter to mysql container run 
```docker-compose exec mysql bash```

Database allows connections only from localhost. 
Because of this when you use the project on production and want to connect to database from your computer
you should connect via ssh bridge.
