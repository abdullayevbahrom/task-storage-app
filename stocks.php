<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login']) {
  header("Location: login.php");
  exit();
}

$page = "Stocks";

$number = 0;
$stocks = Stock::getAllStocks();

function countStock($id, $key)
{
  if ($products = Pivot::getAllCountByStockId($id)) {
    if ($key == 'quantity') {
      $count = 0;

      foreach ($products as $product) {
        $count += $product->$key;
      }

      return $count;
    } else {
      $count = [];

      foreach ($products as $product) {
        if ($product->$key == null) {
          continue;
        } else {
          $count[] = $product->$key;
        }
      }

      return count(array_unique($count));
    }
  }
  return 0;
}



if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["DELETE"]) && isset($_POST["stock_id"])) {
  $stock_id = $_POST["stock_id"];
  Stock::deleteStockById($stock_id);

  unset($_POST["stock_id"]);
  header("Location: stocks.php");
  exit;
}

require 'includes/header.php';
?>
<div class="container mt-5">
    <div class="row tm-content-row">
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 tm-block-col">
            <div class="tm-bg-primary-dark tm-block tm-block-products">
                <div class="tm-product-table-container mt-5">
                    <table class="table table-hover tm-table-small tm-product-table">
                        <thead>
                            <tr>
                                <th scope="col"><b>№</b></th>
                                <th scope="col">STOCK NAME</th>
                                <th scope="col">CATEGORIES</th>
                                <th scope="col">PRODUCTS</th>
                                <th scope="col">IN STOCK</th>
                                <th scope="col">&nbsp;</th>
                                <th scope="col">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($stocks as $stock) : ?>
                            <tr>
                                <td><?= ++$number; ?></td>
                                <td class="tm-product-name"><?= $stock->stock_name ?></td>
                                <td><?= countStock($stock->id, "category_id"); ?></td>
                                <td><?= countStock($stock->id, "product_id"); ?></td>
                                <td><?= countStock($stock->id, "quantity"); ?></td>
                                <td>
                                    <form action="edit-stock.php" method="POST">
                                        <input type="hidden" name="PUT" />
                                        <input type="hidden" name="stock_id" value="<?= $stock->id ?>" />
                                        <button type="submit" class="tm-product-delete-link">
                                            <i class="far fa-edit tm-product-delete-icon"></i>
                                        </button>
                                    </form>
                                </td>
                                <td>
                                    <form action="" method="POST"
                                        onSubmit="return confirm('Rostdan ham o\'chirmoqchimisiz?')">
                                        <input type="hidden" name="DELETE" />
                                        <input type="hidden" name="stock_id" value="<?= $stock->id ?>" />
                                        <button type="submit" class="tm-product-delete-link">
                                            <i class="far fa-trash-alt tm-product-delete-icon"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                </div>
                <a href="add-stock.php" class="btn btn-primary btn-block text-uppercase mb-3">Add new stock</a>
            </div>
        </div>
    </div>
</div>
<?php require 'includes/footer.php';