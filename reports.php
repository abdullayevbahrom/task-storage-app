<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login'] ) {
  header("Location: login.php");
  exit();
}

$page = "Reports";

$number = 0;
$reports = !empty($_POST["date"]) ? Report::getAllReportsByDateAndCategoryId($_POST["category_id"], $_POST["date"]) : Report::getAllReports();
$categories = Category::getAllCategories();

unset($_POST["category_id"], $_POST["date"]);

require 'includes/header.php';
?>
<div class="container-fluid mt-5">
  <div class="row tm-content-row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 tm-block-col">
      <div class="tm-bg-primary-dark tm-block tm-block-products">
        <form action="" method="POST" class="form-inline align-center">
          <label class="my-1 mr-2 text-warning" for="inlineFormCustomSelectPref">Categories</label>
          <select name="category_id" class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
            <option selected>Choose...</option>
            <?php foreach ($categories as $category) : ?>
              <option value="<?= $category->id; ?>"><?= $category->category_name; ?></option>
            <?php endforeach ?>
          </select>
          <div class="form-control my-1 mr-sm-2">
            <input type="date" class="custom-control mr-sm-2" name="date">
          </div>

          <button type="submit" class="btn btn-primary my-1">Submit</button>
        </form>
        <div class="tm-product-table-container mt-5">
          <table class="table table-hover tm-table-small tm-product-table">
            <thead>
              <tr>
                <th scope="col"><b>№</b></th>
                <th scope="col"><b>DATE</b></th>
                <th scope="col">PRODUCT NAME</th>
                <th scope="col">CATEGORY NAME</th>
                <th scope="col">STOCK NAME</th>
                <th scope="col">UNIT SOLD</th>
                <th scope="col">RECEIPT</th>
                <th scope="col">IN STOCK</th>
              </tr>
            </thead>
            <tbody>
              <?php if (!$reports) : ?>
                <tr>Bunday report mavjud emas</tr>
              <?php endif ?>
              <?php foreach ($reports as $report) : ?>
                <tr>
                  <td><?= ++$number; ?></td>
                  <td>
                    <?php $date = new DateTime($report->created_at);
                    echo $new_date_format = $date->format('Y-m-d');
                    ?>
                  </td>
                  <td class="tm-product-name"><?= $report->name; ?></td>
                  <td><?= $report->category_name; ?></td>
                  <td><?= $report->stock_name; ?></td>
                  <td><?= $report->output ?? 0; ?></td>
                  <td><?= $report->input ?? 0; ?></td>
                  <td><?= $report->quantity; ?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require 'includes/footer.php';