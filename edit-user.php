<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login']) {
    header("Location: login.php");
    exit();
}

if ($_COOKIE["role"] !== "admin") {
    header("Location: index.php");
    exit();
  }

$page = "Edit User";

if ($_SERVER["REQUEST_METHOD"] == 'POST' && isset($_POST["PUT"]) && isset($_POST["user_id"]) && isset($_POST["role_id"])) {
    $user_id = intval($_POST["user_id"]);
    $role_id = intval($_POST["role_id"]);

    User::updateUserById($user_id, $role_id);

    unset($_POST["user_id"], $_POST["role_id"]);

    header("Location: accounts.php");
    exit;
}

if ($_SERVER["REQUEST_METHOD"] == 'POST' && isset($_POST["PUT"]) && isset($_POST["user_id"])) {
    $roles = Role::getAllRoles();

    $user_id = $_POST["user_id"];
    $user = User::getUserById($user_id);

    unset($_POST["user_id"]);
}

require 'includes/header.php';
?>
<div class="container tm-mt-big tm-mb-big">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mx-auto">
            <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
                <div class="row">
                    <div class="col-12">
                        <h2 class="tm-block-title d-inline-block">Edit User</h2>
                    </div>
                </div>
                <div class="row tm-edit-product-row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <form action="" method="POST" class="tm-edit-product-form">
                            <input type="hidden" name="PUT" />
                            <input type="hidden" name="user_id" value="<?= $user->id; ?>" />
                            <div class="form-group mb-3">
                                <label for="name">User Name</label>
                                <input id="name" name="username" disabled value="<?= $user->username; ?>" type="text" />
                            </div>
                            <div class="form-group mb-3">
                                <label for="category">Roles</label>
                                <select class="custom-select tm-select-accounts" name="role_id" id="category">
                                    <?php foreach ($roles as $role) : ?>
                                        <option selected value="<?= $role->id; ?>"><?= $role->role_name; ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary btn-block text-uppercase">Update User</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require 'includes/footer.php';
