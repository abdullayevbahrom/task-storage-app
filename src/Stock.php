<?php

class Stock
{
    public static $pdo;

    public static function getAllStocks()
    {
        $stocks = self::$pdo->query("SELECT * FROM `stocks` ORDER BY `stock_name`")->fetchAll(PDO::FETCH_OBJ);

        return $stocks;
    }

    public static function getStockById($id)
    {
        $stmt = self::$pdo->prepare("SELECT * FROM `stocks` WHERE `id` = :id LIMIT 1");
        $stmt->execute(['id' => $id]);
        $stock = $stmt->fetch(PDO::FETCH_OBJ);

        return $stock;
    }

    public static function getStockByName($stock_name)
    {
        $stmt = self::$pdo->prepare("SELECT * FROM `stocks` WHERE `stock_name` = :stock_name LIMIT 1");
        $stmt->execute(['stock_name' => $stock_name]);
        $stock = $stmt->fetch(PDO::FETCH_OBJ);

        return $stock;
    }

    public static function addStock($stock_name)
    {
        $stmt = self::$pdo->prepare("INSERT INTO `stocks` (`stock_name`) VALUES (:stock_name)");
        $stmt->execute(['stock_name' => $stock_name]);
        $stock = self::getStockByName($stock_name);

        return $stock;
    }

    public static function updateStockById($id, $stock_name)
    {
        $stmt = self::$pdo->prepare("UPDATE `stocks` SET `stock_name` = :stock_name WHERE `id` = :id");
        $stock = $stmt->execute(['id' => $id, 'stock_name' => $stock_name]);

        return $stock;
    }

    public static function deleteStockById($id)
    {
        $stmt = self::$pdo->prepare("DELETE FROM `stocks` WHERE `id` = :id");
        $stmt->execute(['id' => $id]);
    }
}
