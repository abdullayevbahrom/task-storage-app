<?php

class Category
{
    public static $pdo;

    public static function getAllCategories()
    {
        $categories = self::$pdo->query("SELECT * FROM `categories`")->fetchAll(PDO::FETCH_OBJ);

        return $categories;
    }

    public static function getCategoryById($id)
    {
        $stmt = self::$pdo->prepare("SELECT * FROM `categories` WHERE `id` = :id LIMIT 1");
        $stmt->execute(['id' => $id]);
        $category = $stmt->fetch(PDO::FETCH_OBJ);

        return $category;
    }

    public static function getCategoryByName($category_name)
    {
        $stmt = self::$pdo->prepare("SELECT * FROM `categories` WHERE `category_name` = :category_name LIMIT 1");
        $stmt->execute(['category_name' => $category_name]);
        $category = $stmt->fetch(PDO::FETCH_OBJ);

        return $category;
    }

    public static function addCategory($name)
    {
        $stmt = self::$pdo->prepare("INSERT INTO `categories` (`name`) VALUES (:name)");
        $category = $stmt->execute(['name' => $name]);

        return $category;
    }

    public static function updateCategoryById($id, $name)
    {
        $stmt = self::$pdo->prepare("UPDATE `categories` SET (`name` = :name) WHERE `id` = :id");
        $stmt->execute(['id' => $id, 'name' => $name]);
        $category = $stmt->fetch(PDO::FETCH_OBJ);

        return $category;
    }

    public static function deleteCategoryById($id)
    {
        $stmt = self::$pdo->prepare("DELETE FROM `categories` WHERE `id` = :id");
        $stmt->execute(['id' => $id]);
    }
}
