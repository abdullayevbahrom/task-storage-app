<?php

class Pivot
{
    public static $pdo;

    public static function addNewStockWithCategory($stock_id, $category_id)
    {
        $stmt = self::$pdo->prepare("INSERT INTO `category_product_stock` (`stock_id`, `category_id`) VALUES (:stock_id, :category_id)");
        $stmt->execute(['stock_id' => $stock_id, 'category_id' => $category_id]);
    }

    public static function getIdByCategoriesAanStockAndProductId($stock_id, $category_id, $product_id)
    {
        $sql = "SELECT `id`
            FROM category_product_stock
            WHERE stock_id = :stock_id AND category_id = :category_id AND product_id = :product_id";

        $stmt = self::$pdo->prepare($sql);
        $stmt->execute([
            'stock_id' => $stock_id,
            'category_id' => $category_id,
            'product_id' => $product_id
        ]);
        $cps = $stmt->fetch(PDO::FETCH_OBJ);

        return $cps;
    }


    public static function getAllCategoriesByStockId($stock_id, $count = false)
    {
        try {
            $sql = "SELECT c.*
            FROM category_product_stock cps
            JOIN stocks s ON s.id = cps.stock_id
            JOIN categories c ON c.id = cps.category_id
            WHERE stock_id = :stock_id
            GROUP BY c.id";

            $stmt = self::$pdo->prepare($sql);
            $stmt->execute(['stock_id' => $stock_id]);
            $categoriesByStock = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $count ? count($categoriesByStock) : $categoriesByStock;
        } catch (\Throwable $th) {
            return null;
        }
    }

    public static function getAllProductsByCategoryId($stock_id, $category_id)
    {
        try {
            $sql = "SELECT p.id, p.name, p.price, expire_date, quantity
            FROM category_product_stock cps
            JOIN products p ON p.id = cps.product_id
            WHERE stock_id = :stock_id AND category_id = :category_id";

            $stmt = self::$pdo->prepare($sql);
            $stmt->execute([
                'stock_id' => $stock_id,
                'category_id' => $category_id,
            ]);
            $productsByCategory = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $productsByCategory;
        } catch (\Throwable $th) {
            return null;
        }
    }

    public static function getAllProductsInStock()
    {
        try {
            $sql = "SELECT *
            FROM category_product_stock cps
            JOIN products p ON p.id = cps.product_id
            JOIN categories c ON c.id = cps.category_id
            JOIN stocks s ON s.id = cps.stock_id
            ORDER BY p.name";

            $stmt = self::$pdo->prepare($sql);
            $stmt->execute();
            $productsByCategory = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $productsByCategory;
        } catch (\Throwable $th) {
            return null;
        }
    }


    public static function getAllCountByStockId($stock_id)
    {
        try {
            $sql = "SELECT *
            FROM category_product_stock cps
            WHERE stock_id = :stock_id";

            $stmt = self::$pdo->prepare($sql);
            $stmt->execute(['stock_id' => $stock_id]);
            $categoriesByStock = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $categoriesByStock;
        } catch (\Throwable $th) {
            return null;
        }
    }

    public static function addProductToStock($stock_id, $category_id, $product_id, $quantity, $expire_date)
    {
        $old_quantity = intval(self::getProductQuantityInStock($stock_id, $category_id, $product_id)->quantity);
        try {
            if ($old_quantity !== null) {
                $new_quantity = intval($old_quantity) + intval($quantity);
                self::updateProductQuantityInStock($stock_id, $category_id, $product_id, $new_quantity);
            } else {
                self::newProductInStock($stock_id, $category_id, $product_id, $quantity, $expire_date);
            }
            $cps_id = self::getIdByCategoriesAanStockAndProductId($stock_id, $category_id, $product_id);
            return $cps_id->id;
        } catch (\Throwable $th) {
            return null;
        }
    }

    public static function removeProductInStock($stock_id, $category_id, $product_id, $quantity)
    {
        $cps_id = self::getIdByCategoriesAanStockAndProductId($stock_id, $category_id, $product_id);

        $old_quantity = self::getProductQuantityInStock($stock_id, $category_id, $product_id)->quantity;

        if ($old_quantity === $quantity) {
            self::deleteProductInStock($stock_id, $category_id, $product_id);
        } else {
            $new_quantity = intval($old_quantity) - intval($quantity);
            self::updateProductQuantityInStock($stock_id, $category_id, $product_id, $new_quantity);
        }
        return $cps_id->id;
    }

    public static function newProductInStock($stock_id, $category_id, $product_id, $quantity, $expire_date)
    {
        $sql = "INSERT INTO `category_product_stock`
        (`stock_id`, `category_id`, `product_id`, `quantity`, `expire_date`)
        VALUES
        (:stock_id, :category_id, :product_id, :quantity, :expire_date)";

        $stmt = self::$pdo->prepare($sql);
        $stmt->execute([
            'stock_id' => $stock_id,
            'category_id' => $category_id,
            'product_id' => $product_id,
            'quantity' => $quantity,
            'expire_date' => $expire_date
        ]);
    }

    public static function getProductQuantityInStock($stock_id, $category_id, $product_id)
    {
        $sql = "SELECT quantity
            FROM category_product_stock
            WHERE stock_id = :stock_id AND category_id = :category_id AND product_id = :product_id";

        $stmt = self::$pdo->prepare($sql);
        $stmt->execute([
            'stock_id' => $stock_id,
            'category_id' => $category_id,
            'product_id' => $product_id
        ]);
        $cps = $stmt->fetch(PDO::FETCH_OBJ);

        return $cps;
    }

    public static function updateProductQuantityInStock($stock_id, $category_id, $product_id, $new_quantity)
    {
        $stmt = self::$pdo->prepare(
            "UPDATE `category_product_stock` 
            SET `quantity` = :new_quantity 
            WHERE `stock_id` = :stock_id AND `category_id` = :category_id AND `product_id` = :product_id"
        );
        $stmt->execute([
            'stock_id' => $stock_id,
            'category_id' => $category_id,
            'product_id' => $product_id,
            'new_quantity' => $new_quantity
        ]);
    }

    public static function updateProductExpireDateInStock($stock_id, $product_id, $expire_date)
    {
        $stmt = self::$pdo->prepare(
            "UPDATE `category_product_stock` 
            SET `expire_date` = :expire_date 
            WHERE `stock_id` = :stock_id AND `product_id` = :product_id"
        );
        $stmt->execute([
            'stock_id' => $stock_id,
            'product_id' => $product_id,
            'expire_date' => $expire_date
        ]);
    }

    public static function deleteStockByCategoryId($stock_id, $category_id)
    {
        $stmt = self::$pdo->prepare("DELETE FROM `category_product_stock` WHERE `stock_id` = :stock_id AND `category_id` = :category_id");
        $stmt->execute(['stock_id' => $stock_id, 'category_id' => $category_id]);
    }

    public static function deleteProductInStock($stock_id, $category_id, $product_id)
    {
        $stmt = self::$pdo->prepare("DELETE FROM `category_product_stock` WHERE `stock_id` = :stock_id AND `category_id` = :category_id AND `product_id` = :product_id");
        $stmt->execute(['stock_id' => $stock_id, 'category_id' => $category_id, 'product_id' => $product_id]);
    }
}
