<?php

class Product
{
    public static $pdo;

    public static function getAllProducts()
    {
        $stmt = self::$pdo->query("SELECT * FROM `products`");
        $products = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $products;
    }

    public static function getProductById($id)
    {
        $stmt = self::$pdo->prepare("SELECT * FROM `products` WHERE `id` = :id LIMIT 1");
        $stmt->execute(['id' => $id]);
        $product = $stmt->fetch(PDO::FETCH_OBJ);

        return $product;
    }

    public static function addProduct($name, $price)
    {
        $stmt = self::$pdo->prepare("INSERT INTO `products` 
            (`name`, `price`) 
            VALUES 
            (:name, :price)");
        $product = $stmt->execute([
            'name' => $name,
            'price' => $price
        ]);

        return $product;
    }

    public static function updateProductById($id, $name, $price)
    {
        $stmt = self::$pdo->prepare("UPDATE `products` SET 
            `name` = :name,
            `price` = :price,
            WHERE `id` = :id");

        $product = $stmt->execute([
            'id' => $id,
            'name' => $name,
            'price' => $price
        ]);

        return $product;
    }

    public static function deleteProductById($id)
    {
        $stmt = self::$pdo->prepare("DELETE FROM `products` WHERE `id` = :id");
        $stmt->execute(['id' => $id]);
    }
}
