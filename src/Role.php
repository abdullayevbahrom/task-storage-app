<?php

class Role
{
    public static $pdo;

    public static function getAllRoles()
    {
        $roles = self::$pdo->query("SELECT * FROM `roles`")->fetchAll(PDO::FETCH_OBJ);

        return $roles;
    }

    public static function getRoleById($id)
    {
        $stmt = self::$pdo->prepare("SELECT * FROM `roles` WHERE `id` = :id LIMIT 1");
        $stmt->execute(['id' => $id]);
        $role = $stmt->fetch(PDO::FETCH_OBJ);

        return $role;
    }

    public static function addRole($role_name)
    {
        $stmt = self::$pdo->prepare("INSERT INTO `roles` (`role_name`) VALUES (:role_name)");
        $role = $stmt->execute(['role_name' => $role_name]);

        return $role;
    }

    public static function updateRoleById($id, $role_name)
    {
        $stmt = self::$pdo->prepare("UPDATE `roles` SET `role_name` = :role_name WHERE `id` = :id");
        $role = $stmt->execute(['id' => $id, 'role_name' => $role_name]);

        return $role;
    }

    public static function deleteRoleById($id)
    {
        $stmt = self::$pdo->prepare("DELETE FROM `roles` WHERE `id` = :id");
        $stmt->execute(['id' => $id]);
    }
}
