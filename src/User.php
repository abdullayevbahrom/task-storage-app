<?php

class User
{
    public static $pdo;

    public static function getAllUsers()
    {
        $sql = "SELECT u.id, u.username, r.role_name AS role
            FROM `users` u
            JOIN roles r ON r.id = u.role_id";
        $stmt = self::$pdo->query($sql);
        $users = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $users;
    }

    public static function getUserById($id)
    {
        $stmt = self::$pdo->prepare("SELECT * FROM `users` WHERE `id` = :id");
        $stmt->execute(['id' => $id]);
        $user = $stmt->fetch(PDO::FETCH_OBJ);

        return $user;
    }

    public static function getUserByUsername($username)
    {
        $sql = "SELECT u.username, r.role_name AS role
            FROM `users` u
            JOIN roles r ON r.id = u.role_id
            WHERE `username` = :username";
        $stmt = self::$pdo->prepare($sql);
        $stmt->execute(['username' => $username]);
        $user = $stmt->fetch(PDO::FETCH_OBJ);

        return $user;
    }

    public static function createUser($username, $password)
    {
        if (self::isExists($username, $password)) {
            return false;
        }

        $hashed_password = password_hash($password, PASSWORD_DEFAULT);
        $role_id = 3;

        try {
            $stmt = self::$pdo->prepare("INSERT INTO `users` 
                (`role_id`, `username`, `password`) 
                VALUES 
                (:role_id, :username, :password)");
            $stmt->execute([
                'role_id' => $role_id,
                'username' => $username,
                'password' => $hashed_password
            ]);

            $user = $stmt->fetch(PDO::FETCH_OBJ);
            return $user;
        } catch (\Throwable $th) {
            return null;
        }
    }

    public static function updateUserById($user_id, $role_id)
    {
        $stmt = self::$pdo->prepare(
            "UPDATE `users` SET 
            `role_id` = :role_id
            WHERE id = :user_id
            ");

        $stmt->execute([
            'user_id' => $user_id,
            'role_id' => $role_id
        ]);

        $user = $stmt->fetch(PDO::FETCH_OBJ);

        return $user;
    }

    public static function deleteUserById($id)
    {
        $stmt = self::$pdo->prepare("DELETE FROM `users` WHERE `id` = :id");
        $stmt->execute(['id' => $id]);
    }

    public static function isExists($username, $password)
    {
        $sql = "SELECT * FROM `users` WHERE username = :username LIMIT 1";
        $stmt = self::$pdo->prepare($sql);
        $stmt->execute(['username' => $username]);
        $user = $stmt->fetch(PDO::FETCH_OBJ);

        if (!$user) {
            return false;
        }

        return password_verify($password, $user->password);
    }
}
