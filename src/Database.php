<?php

class Database
{
    public function __construct(
        private $host,
        private $user,
        private $password,
        private $db
    ) {
    }

    public function connect()
    {
        $charset = 'utf8mb4';

        $dsn = "mysql:host=$this->host;dbname=$this->db;charset=$charset";
        $options = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        ];
        try {
            $pdo = new PDO($dsn, $this->user, $this->password, $options);
            return $pdo;
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }
}
