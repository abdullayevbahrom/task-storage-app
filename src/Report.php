<?php

class Report
{
    public static $pdo;

    public static function addNewReport($cps_id, $input = null, $output = null)
    {
        $date = date('Y-m-d');
        $sql = "INSERT INTO `reports` 
            (`cps_id`, `input`, `output`, `created_at`) 
            VALUES 
            (:cps_id, :input, :output, :date)";
        $stmt = self::$pdo->prepare($sql);
        $stmt->execute(['cps_id' => $cps_id, 'input' => $input, 'output' => $output, 'date' => $date]);
    }


    public static function getAllReportsByDateAndCategoryId($category_id, $created_at)
    {
        try {
            $sql = "SELECT r.id, r.input, r.output, r.created_at, s.stock_name, c.category_name, p.name, p.price, cps.quantity 
            FROM reports r
            JOIN category_product_stock cps ON cps.id = r.cps_id
            JOIN stocks s ON s.id = cps.stock_id
            JOIN categories c ON c.id = cps.category_id
            JOIN products p ON p.id = cps.product_id
            WHERE cps.category_id = :category_id AND r.created_at = :created_at
            ORDER BY r.id DESC";

            $stmt = self::$pdo->prepare($sql);
            $stmt->execute([
                'category_id' => $category_id,
                'created_at' => $created_at
            ]);
            $reports = $stmt->fetchAll(PDO::FETCH_OBJ);

            return $reports;
        } catch (\Throwable $th) {
            return null;
        }
    }

    public static function getAllReports()
    {
        $sql = "SELECT r.id, r.input, r.output, r.created_at, s.stock_name, c.category_name, p.name, p.price, cps.quantity 
            FROM reports r
            JOIN category_product_stock cps ON cps.id = r.cps_id
            JOIN stocks s ON s.id = cps.stock_id
            JOIN categories c ON c.id = cps.category_id
            JOIN products p ON p.id = cps.product_id
            ORDER BY r.id DESC";

        $stmt = self::$pdo->prepare($sql);
        $stmt->execute();
        $report = $stmt->fetchAll(PDO::FETCH_OBJ);

        return $report ?? null;
    }



    // public static function updateProductQuantityInStock($stock_id, $product_id, $quantity)
    // {
    //     $stmt = self::$pdo->prepare(
    //         "UPDATE `category_product_stock` 
    //         SET `quantity` = :quantity 
    //         WHERE `stock_id` = :stock_id AND `product_id` = :product_id"
    //     );
    //     $stmt->execute([
    //         'stock_id' => $stock_id,
    //         'product_id' => $product_id,
    //         'quantity' => $quantity
    //     ]);
    // }


    public static function deleteProductInStock($csp_id, $date)
    {
        $stmt = self::$pdo->prepare("DELETE FROM `reports` WHERE `csp_id` = :csp_id AND `date` = :date");
        $stmt->execute(['csp_id' => $csp_id, 'date' => $date]);
    }
}
