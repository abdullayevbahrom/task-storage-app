<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login']) {
  header("Location: login.php");
  exit();
}

if ($_COOKIE["role"] !== "admin") {
  header("Location: index.php");
  exit();
}

$page = "Add Stock";

$categories = Category::getAllCategories();

if ($_SERVER["REQUEST_METHOD"] == 'POST' && isset($_POST["stock_name"])) {
  $stock_name = $_POST["stock_name"];
  $new_stock = Stock::addStock($stock_name);

  if (isset($_POST["category_ids"])) {
    $category_ids = $_POST["category_ids"];
    foreach ($category_ids as $category_id) {
      Pivot::addNewStockWithCategory($new_stock->id, $category_id);
    }
  }
  unset($_POST["stock_name"], $_POST["category_ids"]);

  header("Location: stocks.php");
  exit;
}

require 'includes/header.php';
?>
<div class="container tm-mt-big tm-mb-big">
  <div class="row">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 mx-auto">
      <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
        <div class="row">
          <div class="col-12">
            <h2 class="tm-block-title d-inline-block">Add New Stock</h2>
          </div>
        </div>
        <div class="row tm-edit-product-row">
          <div class="col-xl-12 col-lg-12 col-md-12">
            <form action="" method="POST" class="tm-edit-product-form">
              <div class="form-group mb-3">
                <label for="name">Stock Name</label>
                <input id="name" name="stock_name" type="text" class="form-control validate" required />
              </div>
              <div class="form-group mb-3">
                <label for="category">Add Category</label>
                <select class="custom-select tm-select-accounts" name="category_ids[]" id="category" multiple>
                  <?php foreach ($categories as $category) : ?>
                    <option value="<?= $category->id; ?>"><?= $category->category_name ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="col-12">
                <button type="submit" class="btn btn-primary btn-block text-uppercase">Add Stock Now</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php require 'includes/footer.php';