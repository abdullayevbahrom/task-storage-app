<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login'] ) {
  header("Location: login.php");
  exit();
}

if ($_COOKIE["role"] == "user") {
  header("Location: index.php");
  exit();
}

$page = "Input Product";

$categories = Category::getAllCategories();
$stocks = Stock::getAllStocks();
$products = Product::getAllProducts();

if (
  $_SERVER["REQUEST_METHOD"] == 'POST' &&
  !empty($_POST["stock_id"]) &&
  !empty($_POST["category_id"]) &&
  !empty($_POST["products"]) &&
  !empty($_POST["expire_dates"]) &&
  !empty($_POST["quantities"])
) {
  $stock_id = intval($_POST["stock_id"]);
  $category_id = intval($_POST["category_id"]);
  $products = $_POST["products"];
  $quantities = $_POST["quantities"];
  $expire_dates = $_POST["expire_dates"];

  $filtered_quantities = array_values(array_filter($quantities, fn ($el) => $el !== "" ? true : false));
  $filtered_expire_dates = array_values(array_filter($expire_dates, fn ($el) => $el !== "" ? true : false));

  for ($i = 0; $i < count($products); $i++) {
    $cps_id = Pivot::addProductToStock($stock_id, $category_id, intval($products[$i]), intval($filtered_quantities[$i]), $filtered_expire_dates[$i]);
    Report::addNewReport($cps_id, intval($filtered_quantities[$i]), null);
  }

  unset($_POST["stock_id"], $_POST["category_id"], $_POST["products"], $_POST["expire_dates"], $_POST["quantities"]);

  header("Location: stocks.php");
  exit;
}

require 'includes/header.php';
?>
<div class="container-fluid mt-3">
  <form action="" method="POST" class="row tm-content-row">
    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 tm-block-col">
      <div class="tm-bg-primary-dark tm-block tm-block-product-categories">
        <div class="tm-product-table-container">
          <table class="table table-hover tm-table-small tm-product-table">
            <thead>
              <tr>
                <th scope="col">&nbsp;</th>
                <th scope="col">STOCK NAME</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($stocks as $stock) : ?>
                <tr>
                  <th scope="row"><input id="<?= $stock->stock_name ?>" type="radio" name="stock_id" value="<?= $stock->id ?>" required /></th>
                  <td class="tm-product-name"><label for="<?= $stock->stock_name ?>"><?= $stock->stock_name ?></label></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-3 col-xl-3 tm-block-col">
      <div class="tm-bg-primary-dark tm-block tm-block-product-categories">
        <div class="tm-product-table-container">
          <table class="table table-hover tm-table-small tm-product-table">
            <thead>
              <tr>
                <th scope="col">&nbsp;</th>
                <th scope="col">CATEGORY NAME</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($categories as $category) : ?>
                <tr>
                  <th scope="row"><input id="<?= $category->category_name ?>" type="radio" name="category_id" value="<?= $category->id ?>" required /></th>
                  <td class="tm-product-name"><label for="<?= $category->category_name ?>"><?= $category->category_name ?></label></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 tm-block-col">
      <div class="tm-bg-primary-dark tm-block tm-block-product-categories">
        <div class="tm-product-table-container">
          <table class="table table-hover tm-table-small tm-product-table">
            <thead>
              <tr>
                <th scope="col">&nbsp;</th>
                <th scope="col">PRODUCT NAME</th>
                <th scope="col">PRICE</th>
                <th scope="col">EXPIRE DATE</th>
                <th scope="col">QUANTITY</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) : ?>
                <tr>
                  <th scope="row"><input id="<?= $product->name ?>" type="checkbox" name="products[]" value="<?= $product->id; ?>" req /></th>
                  <td class="tm-product-name"><label for="<?= $product->name ?>"><?= $product->name; ?></label></td>
                  <td><?= $product->price; ?></td>
                  <td><input type="date" name="expire_dates[]" autocomplete /></td>
                  <td><input type="number" name="quantities[]" autocomplete /></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <button type="submit" class="container col-6 btn btn-primary btn-block text-uppercase">
      Add selected products to Stock
    </button>
  </form>
</div>
<?php require 'includes/footer.php';