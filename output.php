<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login'] ) {
  header("Location: login.php");
  exit();
}

if ($_COOKIE["role"] == "user") {
  header("Location: index.php");
  exit();
}

$page = "Output Product";


$products = Pivot::getAllProductsInStock();

if ($_SERVER["REQUEST_METHOD"] == 'POST' && !empty($_POST["products"]) && !empty($_POST["quantities"])) {
  $products = $_POST["products"];
  $quantities = $_POST["quantities"];

  $filtered_quantities = array_values(array_filter($quantities, fn ($el) => $el !== "" ? true : false));

  for ($i = 0; $i < count($products); $i++) {
    $ids = explode("/", $products[$i]);
    $stock_id = (int)$ids[0];
    $category_id = (int)$ids[1];
    $product_id = (int)$ids[2];

    $cps_id = Pivot::removeProductInStock($stock_id, $category_id, $product_id, intval($filtered_quantities[$i]));
    Report::addNewReport($cps_id, null, intval($filtered_quantities[$i]));
  }

  unset($_POST["stock_id"], $_POST["category_id"], $_POST["products"], $_POST["quantities"]);

  header("Location: stocks.php");
  exit;
}

require 'includes/header.php';
?>
<div class="container-fluid mt-5">
  <form action="" method="POST" class="row tm-content-row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 tm-block-col">
      <div class="tm-bg-primary-dark tm-block tm-block-product-categories">
        <div class="tm-product-table-container">
          <table class="table table-hover tm-table-small tm-product-table">
            <thead>
              <tr>
                <th scope="col">&nbsp;</th>
                <th scope="col">PRODUCT NAME</th>
                <th scope="col">STOCK NAME</th>
                <th scope="col">CATEGORY NAME</th>
                <th scope="col">PRICE</th>
                <th scope="col">EXPIRE DATE</th>
                <th scope="col">IN STOCK</th>
                <th scope="col">QUANTITY</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) : ?>
                <tr>
                  <th scope="row"><input id="<?= $product->name ?>" type="checkbox" name="products[]" value="<?= "$product->stock_id/$product->category_id/$product->product_id"; ?>" /></th>
                  <td class="tm-product-name"><label for="<?= $product->name ?>"><?= $product->name; ?></label></td>
                  <td><?= $product->stock_name; ?></td>
                  <td><?= $product->category_name; ?></td>
                  <td><?= $product->price; ?></td>
                  <td><?= $product->expire_date; ?></td>
                  <td><?= $product->quantity; ?></td>
                  <td><input type="number" name="quantities[]" max="<?= $product->quantity; ?>" /></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <button type="submit" class="container col-6 btn btn-primary btn-block text-uppercase">
          Subtract Selected Products In Stock
        </button>
      </div>
    </div>
  </form>
</div>
<?php require 'includes/footer.php';