<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login'] ) {
  header("Location: login.php");
  exit();
}

$page = "Products";

$number = 1;
$products = Product::getAllProducts();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["DELETE"]) && isset($_POST["product_id"])) {
  $product_id = $_POST["product_id"];
  Product::deleteProductById($product_id);

  unset($_POST["product_id"]);
  header("Location: products.php");
  exit;
}

require 'includes/header.php';
?>
<div class="container mt-5">
  <div class="row tm-content-row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 tm-block-col">
      <div class="tm-bg-primary-dark tm-block tm-block-products">
        <div class="tm-product-table-container mt-5">
          <table class="table table-hover tm-table-small tm-product-table">
            <thead>
              <tr>
                <th scope="col"><b>№</b></th>
                <th scope="col">PRODUCT NAME</th>
                <th scope="col">PRICE</th>
                <th scope="col">&nbsp;</th>
                <th scope="col">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($products as $product) : ?>
                <tr>
                  <th scope="row"><?= $number++; ?></th>
                  <td class="tm-product-name"><?= $product->name ?></td>
                  <td><?= "$ ".$product->price ?></td>
                  <td>
                    <form action="edit-product.php" method="POST">
                      <input type="hidden" name="PUT" />
                      <input type="hidden" name="product_id" value="<?= $product->id ?>" />
                      <button type="submit" class="tm-product-delete-link">
                        <i class="far fa-edit tm-product-delete-icon"></i>
                      </button>
                    </form>
                  </td>
                  <td>
                    <form action="" method="POST" onSubmit="return confirm('Rostdan ham o\'chirmoqchimisiz?')">
                      <input type="hidden" name="DELETE" />
                      <input type="hidden" name="product_id" value="<?= $product->id ?>" />
                      <button type="submit" class="tm-product-delete-link">
                        <i class="far fa-trash-alt tm-product-delete-icon"></i>
                      </button>
                    </form>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <a href="add-product.php" class="btn btn-primary btn-block text-uppercase mb-3">Add new product</a>
      </div>
    </div>
  </div>
</div>
<?php require 'includes/footer.php';