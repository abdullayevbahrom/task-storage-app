<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login']) {
  header("Location: login.php");
  exit();
}
if ($_COOKIE["role"] !== "admin") {
  header("Location: index.php");
  exit();
}

$number = 0;
$page = "Accounts";

$users = User::getAllUsers();

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST["DELETE"]) && isset($_POST["user_id"])) {
  $user_id = intval($_POST["user_id"]);
  User::deleteUserById($user_id);

  unset($_POST["user_id"]);
  header("Location: accounts.php");
  exit;
}


require 'includes/header.php'; ?>
<div class="container mt-5">
  <div class="row tm-content-row">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 tm-block-col">
      <div class="tm-bg-primary-dark tm-block tm-block-products">
        <div class="tm-product-table-container mt-5">
          <table class="table table-hover tm-table-small tm-product-table">
            <thead>
              <tr>
                <th scope="col"><b>№</b></th>
                <th scope="col">USER NAME</th>
                <th scope="col">ROLE</th>
                <th scope="col">&nbsp;</th>
                <th scope="col">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              <?php if (!$users) : ?>
                <tr>Userlar mavjud emas</tr>
              <?php endif ?>
              <?php foreach ($users as $user) : ?>
                <tr>
                  <td><?= ++$number; ?></td>
                  <td class="tm-product-name"><?= $user->username; ?></td>
                  <td><?= $user->role; ?></td>
                  <td>
                    <form action="edit-user.php" method="POST">
                      <input type="hidden" name="PUT" />
                      <input type="hidden" name="user_id" value="<?= $user->id ?>" />
                      <button type="submit" class="tm-product-delete-link">
                        <i class="far fa-edit tm-product-delete-icon"></i>
                      </button>
                    </form>
                  </td>
                  <td>
                    <form action="" method="POST" onSubmit="return confirm('Rostdan ham o\'chirmoqchimisiz?')">
                      <input type="hidden" name="DELETE" />
                      <input type="hidden" name="user_id" value="<?= $user->id ?>" />
                      <button type="submit" class="tm-product-delete-link">
                        <i class="far fa-trash-alt tm-product-delete-icon"></i>
                      </button>
                    </form>
                  </td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require 'includes/footer.php';
