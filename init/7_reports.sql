create table 
    if not exists reports (
        id INT AUTO_INCREMENT PRIMARY KEY,
        cps_id INT NOT NULL,
        input INT,
        output INT,
        created_at DATE,
        FOREIGN KEY(`cps_id`) REFERENCES `category_product_stock`(`id`) ON UPDATE CASCADE ON DELETE CASCADE
    );

