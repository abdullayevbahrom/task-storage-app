create table
    if not exists stocks (
        `id` INT AUTO_INCREMENT PRIMARY KEY,
        `stock_name` VARCHAR(50) NOT NULL UNIQUE
    );

insert into stocks (`stock_name`) values ('Limoneira Co');

insert into
    stocks (`stock_name`)
values (
        'Blackrock Debt Strategies Fund, Inc.'
    );

insert into stocks (`stock_name`) values ('Citigroup Inc.');

insert into stocks (`stock_name`) values ('CME Group Inc.');

insert into stocks (`stock_name`) values ('H. B. Fuller Company');

insert into stocks (`stock_name`) values ('Cerulean Pharma Inc.');

insert into stocks (`stock_name`) values ('FormFactor, Inc.');

insert into
    stocks (`stock_name`)
values (
        'Natural Gas Services Group, Inc.'
    );

insert into stocks (`stock_name`) values ('Caesarstone Ltd.');

insert into
    stocks (`stock_name`)
values (
        'Atlantic Capital Bancshares, Inc.'
    );