create table if not exists users (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `role_id` INT NOT NULL,
    `username` VARCHAR(50) NOT NULL UNIQUE,
    `password` VARCHAR(255) NOT NULL,
    FOREIGN KEY(`role_id`) REFERENCES `roles`(`id`) ON UPDATE CASCADE ON DELETE CASCADE
);

insert into
    users (`role_id`, `username`, `password`)
values
    (
        3,
        'user',
        "$2y$10$GMlRzbS0SpYXfCgXk.deouAoNL3D0WgPTKxOhiKe9zQDNNPAKtPNO"
    );

insert into
    users (`role_id`, `username`, `password`)
values
    (
        1,
        'admin',
        "$2y$10$ihWfNm7.AFTfngtiJm2kmu3fBxQ4tGLsCvC/Btz2aO.SIyCPVvRqK"
    );

insert into
    users (`role_id`, `username`, `password`)
values
    (
        2,
        'manager',
        "$2y$10$yXfdFloeXy0GvvK7IpJf9.DuZGDhoDovGbnGLnHXpUblceTU.rWwm"
    );

