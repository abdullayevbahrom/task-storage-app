create table
    if not exists categories (
        `id` INT AUTO_INCREMENT PRIMARY KEY,
        `category_name` VARCHAR(50) NOT NULL
    );

insert into categories (`category_name`) values ('Computers');

insert into categories (`category_name`) values ('Tools');

insert into categories (`category_name`) values ('Women');

insert into categories (`category_name`) values ('Baby');

insert into categories (`category_name`) values ('Games');

insert into categories (`category_name`) values ('Home');

insert into categories (`category_name`) values ('Kids');

insert into categories (`category_name`) values ('Shoes');

insert into categories (`category_name`) values ('Transports');

insert into categories (`category_name`) values ('IT');

insert into categories (`category_name`) values ('Cosmetics');

insert into categories (`category_name`) values ('Digital Services');

insert into categories (`category_name`) values ('Health');

insert into categories (`category_name`) values ('Books');

insert into categories (`category_name`) values ('Fast Food');

insert into categories (`category_name`) values ('Automotive');

insert into categories (`category_name`) values ('Tools');

insert into categories (`category_name`) values ('Media');

insert into categories (`category_name`) values ('Electronics');

insert into categories (`category_name`) values ('Furniture and Decor');