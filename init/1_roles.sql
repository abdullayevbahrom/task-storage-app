create table
    if not exists roles (
        `id` INT AUTO_INCREMENT PRIMARY KEY,
        `role_name` VARCHAR(50) NOT NULL
    );

insert into roles (`role_name`) values ('admin');

insert into roles (`role_name`) values ('manager');

insert into roles (`role_name`) values ('user');