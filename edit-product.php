<?php

require 'bootstrap.php';

if (!isset($_COOKIE['login']) || !$_COOKIE['login']) {
  header("Location: login.php");
  exit();
}

if ($_COOKIE["role"] == "user") {
  header("Location: index.php");
  exit();
}

$page = "Edit Product";

if (
  $_SERVER["REQUEST_METHOD"] == 'POST' &&
  isset($_POST["PUT"]) &&
  isset($_POST["product_id"]) &&
  isset($_POST["name"]) &&
  isset($_POST["price"])
) {
  $product_id = $_POST["product_id"];
  $name = $_POST["name"];
  $price = $_POST["price"];

  $updated_product = Product::updateProductById($product_id, $name, $price);

  unset($_POST["product_id"], $_POST["name"], $_POST["price"]);

  header("Location: products.php");
  exit;
}

if ($_SERVER["REQUEST_METHOD"] == 'POST' && isset($_POST["PUT"]) && isset($_POST["product_id"])) {
  $product = Product::getProductById($_POST["product_id"]);

  unset($_POST["product_id"]);
}

require 'includes/header.php';
?>
<div class="container tm-mt-big tm-mb-big">
  <div class="row">
    <div class="col-xl-9 col-lg-10 col-md-12 col-sm-12 mx-auto">
      <div class="tm-bg-primary-dark tm-block tm-block-h-auto">
        <div class="row">
          <div class="col-12">
            <h2 class="tm-block-title d-inline-block">Edit Product</h2>
          </div>
        </div>
        <div class="row tm-edit-product-row">
          <div class="col-xl-12 col-lg-12 col-md-12">
            <form action="" method="POST" class="tm-edit-product-form">
              <input type="hidden" name="PUT" />
              <input type="hidden" name="product_id" value="<?= $product->id ?>" />
              <div class="form-group mb-3">
                <label for="name">Name</label>
                <input id="name" name="name" type="text" value="<?= $product->name; ?>" class="form-control validate" />
              </div>
              <div class="form-group mb-3">
                <label for="price">Price</label>
                <input id="price" name="price" type="text" value="<?= $product->price; ?>" class="form-control validate" />
              </div>
              <div>
                <button type="submit" class="btn btn-primary btn-block text-uppercase">Update Now</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require 'includes/footer.php';
